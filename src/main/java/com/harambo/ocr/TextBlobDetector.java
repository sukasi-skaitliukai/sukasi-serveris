package com.harambo.ocr;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.CLAHE;
import org.opencv.imgproc.Imgproc;

public class TextBlobDetector {
	
	
	public static Mat FindText(Mat im, String file) {
		Mat org = im;
		Mat large = im;
		Mat rgb = new Mat();
		Imgproc.cvtColor(large, rgb, Imgproc.COLOR_BGR2GRAY);
		Imgproc.pyrDown(rgb, rgb);
		
		CLAHE cl = Imgproc.createCLAHE(4.0, new Size(32,32));
		cl.apply(rgb, rgb);
		
		Mat bw = new Mat();
		Imgproc.threshold(rgb, bw, 0, 255.0,  Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
		float r = (float)(Core.countNonZero(bw) / (float)(bw.size().width * bw.size().height));
		System.out.println(r + " " + file);
		
		if(r > 0.70) {
			Core.bitwise_not(bw, bw);
		}
		
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat hierarchy = new Mat();
		Mat c = Mat.zeros(bw.size(), CvType.CV_8UC1);
		Imgproc.findContours(bw, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_TC89_L1 );
		
		Mat mask = Mat.zeros(bw.size(), CvType.CV_8UC1);
		
		for(int i = 0 ; i < contours.size(); i++) {
			Rect x = Imgproc.boundingRect(contours.get(i));
			
			Imgproc.drawContours(c, contours, i, new Scalar(255, 0, 0), -1);
			
			Mat sub = c.submat(x);
			r = (float)(Core.countNonZero(sub) / (float)(x.width * x.height));
			float hwr = x.height/x.width;
			if(r > 0.2 && x.width > 16  && hwr > 0.5f) {
				//Imgproc.rectangle(large, new Point(x.x, x.y), new Point(x.x+ x.width-1, x.y + x.height-1),new Scalar(0, 255, 0), 2);
				Imgproc.rectangle(mask, new Point(x.x, x.y), new Point(x.x+ x.width-1, x.y + x.height-1),new Scalar(255, 0, 0), -1);
			} else {
				//Imgproc.rectangle(large, new Point(x.x, x.y), new Point(x.x+ x.width-1, x.y + x.height-1),new Scalar(255, 0, 0), 1);
			}
		}
		Core.bitwise_and(bw, mask, rgb);
		//Core.bitwise_not(rgb, rgb);
		
	    Size size = rgb.size();
	    Core.bitwise_not(rgb, rgb);
	    Mat lines = new Mat();
	    Imgproc.HoughLinesP(rgb, lines, 1, Math.PI / 180, 100, size.width / 2.f, 20);
	    double angle = 0.;
	    for(int i = 0; i<lines.height(); i++){
	        for(int j = 0; j<lines.width();j++){
	            angle += Math.atan2(lines.get(i, j)[3] - lines.get(i, j)[1], lines.get(i, j)[2] - lines.get(i, j)[0]);
	        }
	    }
	    angle /= lines.size().area();
	    angle = angle * 180 / Math.PI;
		
		large = deskew(rgb, angle);
		System.out.println(angle);
		
		return rgb;
	}
	
	public static Mat FindText0(Mat im, String file) {
		
		Mat large = im;
		Mat rgb = new Mat();
		//Imgproc.pyrDown(large, rgb);
		rgb = large;
		Mat small = new Mat();
		Imgcodecs.imwrite("c_.png", rgb);

		Imgproc.cvtColor(rgb, small, Imgproc.COLOR_BGR2GRAY);
		Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(4,4));
		Mat grad = new Mat();
		//Imgproc.morphologyEx(small, grad, Imgproc.MORPH_GRADIENT, kernel);
		//Imgproc.Canny(small, grad, 100, 200);
		//Imgcodecs.imwrite("c_.png", grad);
		grad = small;
		
		Mat bw = new Mat();
		Imgproc.threshold(grad, bw, 200, 255.0,  Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
		//Imgproc.adaptiveThreshold(grad, bw, 40, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV, 11, 12);
		
		kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(9,1));
		
		Mat conncted = new Mat();
		Imgproc.morphologyEx(bw, conncted, Imgproc.MORPH_CLOSE, kernel);
		
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat hierarchy = new Mat();
		Mat c = Mat.zeros(bw.size(), CvType.CV_8UC1);
		Imgproc.findContours(conncted, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);
		Imgproc.drawContours(c, contours, 0, new Scalar(0, 0, 0));
		Mat mask = Mat.zeros(bw.size(), CvType.CV_8UC1);
		
		for (int i = 0; i < contours.size(); i++) {
			Rect x = Imgproc.boundingRect(contours.get(i));
			Imgproc.drawContours(mask, contours, i, new Scalar(255, 255, 255), -1);
			Imgproc.drawContours(c, contours, i, new Scalar(255, 0, 0), -1);
			
			Mat sub = mask.submat(x);
			float r = (float)(Core.countNonZero(sub) / (float)(x.width * x.height));
			if( r > 0.45 && x.width > 16 && x.height >= x.width) {
				Imgproc.rectangle(rgb, new Point(x.x, x.y), new Point(x.x+ x.width-1, x.y + x.height-1),new Scalar(0, 255, 0), 2);
			}	
		}
		Imgcodecs.imwrite(file, bw);
		Imgcodecs.imwrite("i_.png", rgb);
		Core.bitwise_not(large, large);
			
		return large;
	}
	
	public static Mat FindText (String filepath) {
		System.out.println(filepath);
		Mat image = Imgcodecs.imread(filepath);
		return TextBlobDetector.FindText(image, "");
	}
	
	public static Mat FindText(byte[] ba) {
		Mat image = ba2Mat(ba);
		return TextBlobDetector.FindText(image, "");
	}
	
	public static Mat ba2Mat (byte[] ba) {
		 MatOfByte matob = new MatOfByte(ba);
		 Mat mat = Imgcodecs.imdecode(matob, Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
		 return mat;
	}
	
	private static  Mat deskew(Mat src, double angle) {
	    Point center = new Point(src.width()/2, src.height()/2);
	    Mat rotImage = Imgproc.getRotationMatrix2D(center, angle, 1.0);
	    Size size = new Size(src.width(), src.height());
	    Imgproc.warpAffine(src, src, rotImage, size, Imgproc.INTER_LINEAR + Imgproc.CV_WARP_FILL_OUTLIERS);
	    return src;
	}
}

package com.univeras.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.univeras.jpa.model.Account;

public class UserDetailsImpl implements UserDetails {

	private static final long serialVersionUID = 1L;

	public static final String ROLE_ADMINISTRATOR = "ROLE_ADMINISTRATOR";

	public static final String ROLE_USER = "ROLE_USER";

	private String password;

	private String username;

	private Set<GrantedAuthority> authorities;

	private UserDetailsImpl() {
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	public static UserDetailsImpl fromAccount(Account account) {
		UserDetailsImpl userDetails = new UserDetailsImpl();
		userDetails.setPassword(account.getPassword());
		userDetails.setUsername(account.getEmail());
		Set<GrantedAuthority> authorities = new HashSet<>();
		for (String authority : account.getAuthorities()) {
			authorities.add(new SimpleGrantedAuthority(authority));
		}
		userDetails.setAuthorities(authorities);
		return userDetails;
	}

}

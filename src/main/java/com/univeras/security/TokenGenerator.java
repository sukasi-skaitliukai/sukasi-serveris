package com.univeras.security;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.springframework.stereotype.Component;

@Component
public class TokenGenerator {
	
	private SecureRandom random = new SecureRandom();

	public synchronized String nextToken() {
		return new BigInteger(130, random).toString(32);
	}
}
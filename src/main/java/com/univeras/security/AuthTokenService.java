package com.univeras.security;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuthTokenService {

	public static final int TOKEN_LIFESPAN_MINS = 60 * 24;

	@Autowired
	private TokenGenerator generator;

	private Map<String, TokenInfo> tokenMap = new ConcurrentHashMap<>();

	private class TokenInfo {
		private final String userName;
		private Date validUntil;

		private TokenInfo(String userName, Date validUntil) {
			this.userName = userName;
			this.validUntil = validUntil;
		}
	}

	public String generateAuthToken(String userName) {
		String token = generator.nextToken();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, TOKEN_LIFESPAN_MINS);
		tokenMap.put(token, new TokenInfo(userName, calendar.getTime()));
		return token;
	}

	public String verifyAuthToken(String token) {
		try {
			String[] parts = token.split(" ");
			String authToken = parts[1];
			TokenInfo tokenInfo = tokenMap.get(authToken);
			if (tokenInfo != null && !tokenInfo.validUntil.before(DateUtils.getCurrentTime())) {
				return tokenInfo.userName;
			}
			return null;
		} catch (Exception e) {
			return null;
		}
	}

}

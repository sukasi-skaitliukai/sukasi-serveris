package com.univeras.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.univeras.filter.CorsFilter;

public class AuthenticationTokenFilter extends OncePerRequestFilter {

	@Autowired
	private CorsFilter corsFilter;

	@Autowired
	private AuthTokenService tokenService;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	private final String tokenHeader = "Authorization";

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		String authToken = request.getHeader(tokenHeader);

		if (authToken != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			String userName = tokenService.verifyAuthToken(authToken);
			if (userName != null) {
				UserDetails userDetails = userDetailsService.loadUserByUsername(userName);
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		}

		if (request.getMethod().equals("OPTIONS")) {
			corsFilter.doFilter(request, response, chain);
		} else {
			chain.doFilter(request, response);
		}
	}

}

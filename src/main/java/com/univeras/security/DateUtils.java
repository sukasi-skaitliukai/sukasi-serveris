package com.univeras.security;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.springframework.stereotype.Component;

@Component
public final class DateUtils {

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss.SSS", Locale.getDefault());

	public static final Date parse(String date) throws ParseException {
		return DATE_FORMAT.parse(date);
	}

	public static Date getCurrentTime() {
		return Calendar.getInstance().getTime();
	}

}

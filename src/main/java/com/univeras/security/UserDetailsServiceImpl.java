package com.univeras.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.univeras.jpa.model.Account;
import com.univeras.jpa.repository.AccountRepository;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private AccountRepository accountRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = accountRepository.getAccountByEmail(username);
		if (account == null) {
			throw new UsernameNotFoundException("User not found");
		} else if (!account.isActivated()) {
			throw new UsernameNotFoundException("Account not activated");
		} else {
			return UserDetailsImpl.fromAccount(account);
		}
	}

}

package com.univeras.rest.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.univeras.rest.model.BasicRequest;
import com.univeras.rest.model.BasicResponse;
import com.univeras.rest.model.HistoryEntryDetails;
import com.univeras.rest.service.GlobalService;

@Controller
public class GlobalController {

	@Autowired
	private GlobalService globalService;

	public BasicResponse processImageRequest(byte[] request) {
		return globalService.processImageRequest(request);
	}

	public BasicResponse saveNewEntry(HistoryEntryDetails details) {
		return globalService.saveNewEntry(details);
	}

	public Set<HistoryEntryDetails> getHistory() {
		return globalService.getHistory();
	}

}

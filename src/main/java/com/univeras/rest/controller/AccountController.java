package com.univeras.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.univeras.rest.model.BasicResponse;
import com.univeras.rest.model.LoginDetails;
import com.univeras.rest.model.RegistrationDetails;
import com.univeras.rest.service.AccountService;

@Component
public class AccountController {

	@Autowired
	private AccountService accountService;

	public BasicResponse register(RegistrationDetails details) {
		return accountService.register(details);
	}

	public BasicResponse login(LoginDetails details) {
		return accountService.login(details);
	}

}

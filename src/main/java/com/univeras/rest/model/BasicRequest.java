package com.univeras.rest.model;

public class BasicRequest {

	private String image;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}

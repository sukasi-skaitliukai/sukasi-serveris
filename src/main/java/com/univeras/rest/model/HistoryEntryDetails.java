package com.univeras.rest.model;

import java.util.Date;

import com.univeras.jpa.model.HistoryEntry.CounterType;

public class HistoryEntryDetails {

	private Date date;

	private String recognizedNumbers;

	private String manualNumbers;

	private byte[] image;

	private CounterType counterType;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getRecognizedNumbers() {
		return recognizedNumbers;
	}

	public void setRecognizedNumbers(String recognizedNumbers) {
		this.recognizedNumbers = recognizedNumbers;
	}

	public String getManualNumbers() {
		return manualNumbers;
	}

	public void setManualNumbers(String manualNumbers) {
		this.manualNumbers = manualNumbers;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public CounterType getCounterType() {
		return counterType;
	}

	public void setCounterType(CounterType counterType) {
		this.counterType = counterType;
	}

}

package com.univeras.rest.model;

public class BasicResponse {

	public enum Status {
		SUCCESS, FAILED
	}

	private Status status;
	private String message;

	public BasicResponse(Status status, String message) {
		this.status = status;
		this.message = message;
	}
	
	public BasicResponse() {
		this.status = Status.SUCCESS;
		this.message = "";
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

package com.univeras.rest;

import java.lang.annotation.Annotation;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Path;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/rs")
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		registerAnnotated(Path.class);
		registerAnnotated(Provider.class);
	}

	private void registerAnnotated(Class<? extends Annotation> annotationType) {
		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
		scanner.addIncludeFilter(new AnnotationTypeFilter(annotationType));
		for (BeanDefinition beanDefinition : scanner.findCandidateComponents("com.univeras.rest")) {
			try {
				register(Class.forName(beanDefinition.getBeanClassName()));
			} catch (ClassNotFoundException ex) {
				// ignore
			}
		}
	}

}
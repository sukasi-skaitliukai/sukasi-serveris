package com.univeras.rest.resource;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.univeras.rest.controller.GlobalController;
import com.univeras.rest.model.BasicRequest;
import com.univeras.rest.model.HistoryEntryDetails;

@Component
@Consumes("application/json")
@Produces("application/json")
@Path("/")
public class GlobalResource {

	private static final Logger logger = Logger.getLogger(GlobalResource.class);

	@Autowired
	private GlobalController globalController;

	@POST
	@Path("/process")
	public Response processImage(BasicRequest request) {
		if (request.getImage() == null) {
			return Response.status(415).build();
		}
		byte[] imageArray = null;
		try {
			imageArray = Base64.getDecoder().decode(new String(request.getImage()).getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			logger.error("Error parsing image", e);
		}
		return Response.ok(globalController.processImageRequest(imageArray)).build();
	}

	@POST
	@Path("/history")
	public Response saveNewEntry(@NotNull @Valid HistoryEntryDetails details) {
		return Response.ok(globalController.saveNewEntry(details)).build();
	}

	@GET
	@Path("/history")
	public Response getHistory() {
		return Response.ok(globalController.getHistory()).build();
	}

}

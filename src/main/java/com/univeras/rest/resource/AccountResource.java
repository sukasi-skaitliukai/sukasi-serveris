package com.univeras.rest.resource;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.univeras.rest.controller.AccountController;
import com.univeras.rest.model.LoginDetails;
import com.univeras.rest.model.RegistrationDetails;

@Component
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("/")
public class AccountResource {

	@Autowired
	private AccountController accountController;

	@POST
	@Path("/register")
	public Response register(@NotNull @Valid RegistrationDetails details) {
		return Response.ok(accountController.register(details)).build();
	}

	@POST
	@Path("/login")
	public Response login(@NotNull @Valid LoginDetails details) {
		return Response.ok(accountController.login(details)).build();
	}

}

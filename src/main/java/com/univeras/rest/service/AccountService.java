package com.univeras.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.univeras.jpa.model.Account;
import com.univeras.jpa.repository.AccountRepository;
import com.univeras.rest.model.BasicResponse;
import com.univeras.rest.model.BasicResponse.Status;
import com.univeras.rest.model.LoginDetails;
import com.univeras.rest.model.RegistrationDetails;
import com.univeras.security.AuthTokenService;

@Service
public class AccountService {

	@Autowired
	private AuthTokenService tokenService;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private PasswordEncoder encoder;

	public BasicResponse register(RegistrationDetails details) {
		if (accountRepository.getAccountByEmail(details.getEmail()) != null) {
			return new BasicResponse(Status.FAILED, "Email already in use");
		}

		Account account = new Account();
		account.setActivated(true);
		account.setEmail(details.getEmail());
		account.setPassword(encoder.encode(details.getPassword()));
		account.getAuthorities().add(Account.ROLE_USER);
		accountRepository.save(account);
		return new BasicResponse(Status.SUCCESS, "Registration successful");
	}

	public BasicResponse login(LoginDetails details) {
		Account account = accountRepository.getAccountByEmail(details.getEmail());
		if (account == null) {
			return new BasicResponse(Status.FAILED, "Account not found");
		}
		if (!account.isActivated()) {
			return new BasicResponse(Status.FAILED, "Account is not activated");
		}
		String psw1 = details.getPassword();
		String psw2 = account.getPassword();

		if (encoder.matches(psw1, psw2)) {
			String authToken = tokenService.generateAuthToken(account.getEmail());
			return new BasicResponse(Status.SUCCESS, authToken);
		}
		return new BasicResponse(Status.FAILED, "Bad credentials");
	}

}

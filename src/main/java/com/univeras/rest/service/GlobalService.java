package com.univeras.rest.service;

import java.io.File;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.harambo.ocr.RandomStringGenerator;
import com.harambo.ocr.TextBlobDetector;
import com.univeras.jpa.model.Account;
import com.univeras.jpa.model.HistoryEntry;
import com.univeras.jpa.repository.AccountRepository;
import com.univeras.jpa.repository.HistoryRepository;
import com.univeras.rest.model.BasicResponse;
import com.univeras.rest.model.BasicResponse.Status;
import com.univeras.rest.model.HistoryEntryDetails;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;


@Component
public class GlobalService {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private HistoryRepository historyRepository;

	public BasicResponse processImageRequest(byte[] request) {
		
		
		String path = RandomStringGenerator.generateString() +".jpg";
		Imgcodecs.imwrite(path, TextBlobDetector.ba2Mat(request));
		
		
		Mat processed = TextBlobDetector.FindText(request);
				
		Imgcodecs.imwrite("_"+path, processed);
		
		ITesseract instance = new Tesseract();  // JNA Interface Mapping
		instance.setTessVariable("tessedit_char_whitelist", "0123456789");
		
		File imageFile = new File("_"+path);
		
		
        BasicResponse response = new BasicResponse();
		
		 try {
	            String result = instance.doOCR(imageFile);
	            result = result.replaceAll("[^0-9]","");
	            System.out.println(result);
				response.setMessage(result);
				return response;
				
	        } catch (TesseractException e) {
	            System.err.println(e.getMessage());
	            response.setMessage("NO GO TESS!");
	            response.setStatus(BasicResponse.Status.FAILED);
	        }
			return response;
		//response.setImage(Base64.getEncoder().encodeToString(responseMat.toArray()));

	}

	public BasicResponse saveNewEntry(HistoryEntryDetails details) {
		String email = SecurityContextHolder.getContext().getAuthentication().getName();
		Account account = accountRepository.getAccountByEmail(email);
		HistoryEntry historyEntry = new HistoryEntry();
		historyEntry.setAccount(account);
		historyEntry.setCounterType(details.getCounterType());
		historyEntry.setDate(new Date());
		historyEntry.setImage(details.getImage());
		historyEntry.setManualNumbers(details.getManualNumbers());
		historyEntry.setRecognizedNumbers(details.getRecognizedNumbers());
		historyRepository.save(historyEntry);
		return new BasicResponse(Status.SUCCESS, "Entry successfully saved");
	}

	public Set<HistoryEntryDetails> getHistory() {
		String email = SecurityContextHolder.getContext().getAuthentication().getName();
		Account account = accountRepository.getAccountByEmail(email);
		Set<HistoryEntryDetails> history = new HashSet<HistoryEntryDetails>();
		for (HistoryEntry entry : account.getHistory()) {
			HistoryEntryDetails details = new HistoryEntryDetails();
			details.setCounterType(entry.getCounterType());
			details.setDate(entry.getDate());
			details.setImage(entry.getImage());
			details.setManualNumbers(entry.getManualNumbers());
			details.setRecognizedNumbers(entry.getRecognizedNumbers());
			history.add(details);
		}
		return history;
	}
	
}



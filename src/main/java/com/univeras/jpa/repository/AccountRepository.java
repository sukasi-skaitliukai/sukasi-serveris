package com.univeras.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.univeras.jpa.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

	public Account getAccountByEmail(String email);
}

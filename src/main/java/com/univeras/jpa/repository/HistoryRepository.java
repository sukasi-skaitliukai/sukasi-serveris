package com.univeras.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.univeras.jpa.model.HistoryEntry;

@Repository
public interface HistoryRepository extends JpaRepository<HistoryEntry, Long> {

}

package com.univeras.jpa.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class HistoryEntry {

	public enum CounterType {
		ELEKTRA, DUJOS, VANDUO
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne
	private Account account;

	private Date date;

	private String recognizedNumbers;

	private String manualNumbers;

	private byte[] image;

	private CounterType counterType;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getRecognizedNumbers() {
		return recognizedNumbers;
	}

	public void setRecognizedNumbers(String recognizedNumbers) {
		this.recognizedNumbers = recognizedNumbers;
	}

	public String getManualNumbers() {
		return manualNumbers;
	}

	public void setManualNumbers(String manualNumbers) {
		this.manualNumbers = manualNumbers;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public CounterType getCounterType() {
		return counterType;
	}

	public void setCounterType(CounterType counterType) {
		this.counterType = counterType;
	}

}

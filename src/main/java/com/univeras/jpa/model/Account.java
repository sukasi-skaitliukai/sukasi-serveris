package com.univeras.jpa.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Account implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String ROLE_ADMINISTRATOR = "ROLE_ADMINISTRATOR";

	public static final String ROLE_USER = "ROLE_USER";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	@Column(unique = true)
	private String email;

	@NotNull
	private String password;

	private boolean activated;

	private String activationToken;

	@ElementCollection(fetch = FetchType.EAGER)
	@Column(name = "authority", nullable = false)
	private Set<String> authorities = new HashSet<String>();

	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<HistoryEntry> history = new HashSet<HistoryEntry>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public String getActivationToken() {
		return activationToken;
	}

	public void setActivationToken(String activationToken) {
		this.activationToken = activationToken;
	}

	public Set<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<String> authorities) {
		this.authorities = authorities;
	}

	public Set<HistoryEntry> getHistory() {
		return history;
	}

	public void setHistory(Set<HistoryEntry> history) {
		this.history = history;
	}

}
